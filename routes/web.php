<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function(){
	Route::resource('news', 'News');
	Route::post('news/getNews', 'News@getNews')->name('news.getNews');
	Route::get('news/{news}/status', 'News@status')->name('news.status');

	Route::resource('users', 'Users');
	Route::post('users/getUsers', 'Users@getUsers')->name('users.getUsers');
	Route::get('users/{user}/status', 'Users@status')->name('users.status');
});
