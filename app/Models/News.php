<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

use App\User;

class News extends Model implements HasMedia
{
    use InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'keywords', 'is_approved',
    ];

    public function registerMediaCollections(): void {
        $this->addMediaCollection('image')
             ->singleFile();
    }

    /**
     * Get the user of the news.
     */
    public function user(){
        return $this->belongsTo(User::class)->withDefault();
    }
}
