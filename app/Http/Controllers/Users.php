<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use DataTables;

class Users extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsers(Request $request){
        $users = User::query()->whereHas('roles', function($query){
            $query->whereName(config('constants.ROLE_TYPE_USER'));
        });

        return DataTables::of($users)
            ->editColumn('created_at', function($user){
                return date(config('constants.DATETIME_FORMAT'), strtotime($user->created_at));
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                $query->whereRaw("LOWER(DATE_FORMAT(created_at,'".config('constants.MYSQL_DATETIME_FORMAT')."')) like ?", ["%$keyword%"]);
            })
            ->editColumn('is_active', function ($user) {
                if($user->is_active == TRUE ){
                    return "<a href='".route('users.status',$user)."'><span class='badge badge-success'>Yes</span></a>";
                }else{
                    return "<a href='".route('users.status',$user)."'><span class='badge badge-danger'>No</span></a>";

                }
            })
            ->rawColumns(['is_active'])
            ->make(true);
    }

    /**
     * Change status the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, User $user){
        if (isset($user->is_active) && $user->is_active==FALSE) {
            $user->update(['is_active'=>TRUE]);
        }else{
            $user->update(['is_active'=>FALSE]);
        }
        return redirect()->route('users.index')->withStatus('Record updated successfully.');
    }
}
