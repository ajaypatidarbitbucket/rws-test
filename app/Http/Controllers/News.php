<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News as NewsModel;

use DataTables;
use Form;

class News extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('news.index');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNews(Request $request){
        if(auth()->user()->hasRole(config('constants.ROLE_TYPE_SUPERADMIN')))
            $news = NewsModel::query()->with(['user']);
        else
            $news = NewsModel::query()->with(['user'])->where('user_id', auth()->id());

        return DataTables::of($news)
            ->editColumn('created_at', function($news){
                return date(config('constants.DATETIME_FORMAT'), strtotime($news->created_at));
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                $query->whereRaw("LOWER(DATE_FORMAT(created_at,'".config('constants.MYSQL_DATETIME_FORMAT')."')) like ?", ["%$keyword%"]);
            })
            ->editColumn('keywords', function($news){
                return implode(', ', explode(',', $news->keywords));
            })
            ->editColumn('is_approved', function ($news) {
                if($news->is_approved == TRUE ){
                    if(auth()->user()->hasRole(config('constants.ROLE_TYPE_SUPERADMIN')))
                        return "<a href='".route('news.status',$news)."'><span class='badge badge-success'>Yes</span></a>";
                    else
                        return "<span class='badge badge-success'>Yes</span>";
                }else{
                    if(auth()->user()->hasRole(config('constants.ROLE_TYPE_SUPERADMIN')))
                        return "<a href='".route('news.status',$news)."'><span class='badge badge-danger'>No</span></a>";
                    else
                        return "<span class='badge badge-danger'>No</span>";

                }
            })
            ->addColumn('actions', function ($news) {
                return
                        // Edit
                        '<a href="'.route('news.edit',[$news->id]).'" class="btn btn-primary">Edit</a> '.
                        // Delete
                        Form::open(array(
                          'style' => 'display: inline-block;',
                          'method' => 'DELETE',
                           'onsubmit'=>"return confirm('Do you really want to delete?')",
                          'route' => ['news.destroy', $news->id])).
                        ' <button type="submit" class="btn btn-danger">Delete</button>'.
                        Form::close();
            })
            ->rawColumns(['is_approved','actions'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:news',
            'description' => 'required',
            'image'=>[
                'file',
                'image',
                'max:'.(config('media-library.max_file_size') / 1024),
            ],
            'keywords' => [
                'required',
                function ($attribute, $keywords, $fail) {
                    if (count(explode(',', $keywords)) < 5) {
                        $fail("Please enter atleast 5 $attribute.");
                    }
                },
            ],
        ]);

        $data = $request->only(['title','description','keywords']);
        $data['user_id'] = auth()->id();
        $news = NewsModel::create($data);

        if ($request->hasFile('image')){
            $file = $request->file('image');
            $customname = time() . '.' . $file->getClientOriginalExtension();
            $news->addMedia($file)
                    ->usingFileName($customname)
                    ->toMediaCollection('image');
        }

        return redirect()->route('news.index')->withStatus('Record saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NewsModel  $news
     * @return \Illuminate\Http\Response
     */
    public function show(NewsModel $news)
    {
        //
    }

    /**
     * Change status the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NewsModel  $news
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, NewsModel $news){
        if (isset($news->is_approved) && $news->is_approved==FALSE) {
            $news->update(['is_approved'=>TRUE]);
        }else{
            $news->update(['is_approved'=>FALSE]);
        }
        return redirect()->route('news.index')->withStatus('Record updated successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NewsModel  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(NewsModel $news)
    {
        return view('news.form',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NewsModel  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewsModel $news)
    {
        $request->validate([
            'title' => 'required|unique:news,title,'.$news->id,
            'description' => 'required',
            'image'=>[
                'file',
                'image',
                'max:'.(config('media-library.max_file_size') / 1024),
            ],
            'keywords' => [
                'required',
                function ($attribute, $keywords, $fail) {
                    if (count(explode(',', $keywords)) < 5) {
                        $fail("Please enter at least 5 $attribute.");
                    }
                },
            ],
        ]);

        $data = $request->only(['title','description','keywords']);
        $news->update($data);

        if ($request->hasFile('image')){
            $file = $request->file('image');
            $customname = time() . '.' . $file->getClientOriginalExtension();
            $news->addMedia($file)
                    ->usingFileName($customname)
                    ->toMediaCollection('image');
        }

        return redirect()->route('news.index')->withStatus('Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NewsModel  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewsModel $news)
    {
        $news->delete();
        return redirect()->route('news.index')->withStatus('Record deleted successfully.');
    }
}
