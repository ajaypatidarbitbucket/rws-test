<?php
	return [
		//User Role Ids
		'ROLE_TYPE_SUPERADMIN' => 'admin',
		'ROLE_TYPE_USER' => 'user',

		// Default Datetime format
		'DATETIME_FORMAT' => 'd M Y, h:i A',
		'MYSQL_DATETIME_FORMAT' => '%d %b %Y, %h:%i %p',
	];
?>