<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $superAdminRole = Role::updateOrCreate(['name' => config('constants.ROLE_TYPE_SUPERADMIN')]);
        $userRole = Role::updateOrCreate(['name' => config('constants.ROLE_TYPE_USER')]);

        $user = User::updateOrCreate(['email'=>'administrator@example.com'],['name'=>'Super Admin',
                                                                     'email_verified_at' => now(),
                                                					 'password'=>Hash::make('password')]);

        $user->assignRole($superAdminRole);

    }
}
