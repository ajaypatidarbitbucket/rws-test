@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Users') }}
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" id="users">
                        <thead>
                            <tr>
                                <th>Created At</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="{{ asset('js/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    getUsers();
});
 
function getUsers(){
    jQuery('#users').dataTable().fnDestroy();
    jQuery('#users tbody').empty();
    jQuery('#users').DataTable({
        processing: false,
        serverSide: true,
        ajax: {
            url: '{{ route("users.getUsers") }}',
            method: 'POST'
        },
        columns: [
            {data: 'created_at', name: 'created_at'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'mobile_number', name: 'mobile_number'},
            {data: 'is_active', name: 'is_active', class: 'text-center'}
        ],
        order: [[0, 'asc']]
    });
}
</script>
@endsection
