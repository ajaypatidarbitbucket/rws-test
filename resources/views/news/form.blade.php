@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Add News') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ isset($news->id)?route('news.update',$news->id):route('news.store') }}" id="frmNews" enctype="multipart/form-data">
                        @csrf
                        @if(isset($news->id))
                        @method('PUT')
                        @endif

                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-8">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title', isset($news->title)?$news->title:'') }}" required autocomplete="title" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-2 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-8">
                                <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" rows="10" required>{{ old('description', isset($news->description)?$news->description:'') }}</textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        @if(isset($news) && $news->getMedia('image')->count() > 0 && file_exists($news->getFirstMedia('image')->getPath()))
                        <div class="form-group row">
                            <label for="image" class="col-md-2 col-form-label text-md-right">&nbsp;</label>
                            <div class="col-md-2">
                                <img class="img-fluid" src="{{ $news->getFirstMedia('image')->getFullUrl() }}">
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="image" class="col-md-2 col-form-label text-md-right">{{ __('Image') }}</label>

                            <div class="col-md-8">
                                <input id="image" type="file" class="@error('image') is-invalid @enderror" name="image" value="">

                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="keywords" class="col-md-2 col-form-label text-md-right">{{ __('Keywords') }}</label>

                            <div class="col-md-8">
                                <input id="keywords" type="text" class="form-control @error('keywords') is-invalid @enderror" name="keywords" value="{{ old('keywords', isset($news->keywords)?$news->keywords:'') }}"  data-role="tagsinput" required>

                                @error('keywords')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('js/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
<style type="text/css">
.bootstrap-tagsinput {
    width: 100%;
}
</style>
@endsection

@section('scripts')
<script src="{{ asset('js/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('#keywords').tagsinput();

    jQuery('.bootstrap-tagsinput input').keydown(function( event ) {
        if ( event.which == 13 ) {
            jQuery(this).blur();
            jQuery(this).focus();
            return false;
        }
    });

    jQuery.validator.addMethod("minTagsInput", function(value, element) {
        return $(element).tagsinput('items').length >= 5;
    }, "Please enter at least 5 keywords.");

    jQuery('#frmNews').validate({
        rules: {
            title: {
                required: true,
            },
            description: {
                required: true,
            },
            keywords: {
                required: true,
                minTagsInput: true
            }
        }
    });
});
</script>
@endsection
