@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    @role(config('constants.ROLE_TYPE_USER'))
                    <a href="{{ route('news.create') }}" class="btn btn-primary float-right">Add</a>
                    @endrole
                    {{ __('News') }}
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" id="news">
                        <thead>
                            <tr>
                                <th>Created At</th>
                                @role(config('constants.ROLE_TYPE_SUPERADMIN'))
                                <th>User</th>
                                @endrole
                                <th>Title</th>
                                <th>Keywords</th>
                                <th>Approved</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="{{ asset('js/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    getNews();
});
 
function getNews(){
    jQuery('#news').dataTable().fnDestroy();
    jQuery('#news tbody').empty();
    jQuery('#news').DataTable({
        processing: false,
        serverSide: true,
        ajax: {
            url: '{{ route("news.getNews") }}',
            method: 'POST'
        },
        columns: [
            {data: 'created_at', name: 'created_at'},
            @role(config('constants.ROLE_TYPE_SUPERADMIN'))
            {data: 'user.name', name: 'user.name'},
            @endrole
            {data: 'title', name: 'title'},
            {data: 'keywords', name: 'keywords'},
            {data: 'is_approved', name: 'is_approved', class: 'text-center'},
            {data: 'actions', name: 'actions', orderable: false, searchable: false},
        ],
        order: [[0, 'asc']]
    });
}
</script>
@endsection
